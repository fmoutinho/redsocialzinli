/* eslint-disable react/jsx-no-undef */
/* eslint-disable no-const-assign */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect, Fragment } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import Global from './UrlApi';
import Main from './Main.js'
import { User } from './User';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { Typography,Badge,Backdrop,Popover,CircularProgress, Grid, Box ,LinearProgress, Tooltip, Tabs, Tab, Divider} from '@material-ui/core'
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
}));
class Login extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
        value: '',
        isLoginState:false,
        submitted: false,
        clase : null,
        open : false
      };

      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);

    }

    handleClick(){
      this.setState({open: true});
    }

    handleClose(event,reason){
      if (reason === 'clickaway') {
        return;
      }
      this.setState({open: false});
    }
  
    handleChange(event) {
      this.setState({value: event.target.value});
    }
  

    handleSubmit(event) {
      this.setState({ submitted: true });
      const { dispatch } = this.props;

        const username=this.state.value;
        const user=[];
         const data =[];
         var isLogin= false;
   
         var url = Global.urlAvatars;
         var request= "/user";
            axios.get(url+request).then(res =>{
             user.push(res.data);

                for (let [key, value] of Object.entries(user)) {
                        data.push(value);
                }
                for(let i=0; i< data[0].length;i++){
                    if(data[0][i]['username'] === username){
                        isLogin=true;
                        this.setState({ isLoginState: true });
                        sessionStorage.setItem('username',JSON.stringify(data[0][i]['username']));
                        sessionStorage.setItem('data',data[0]);
                        localStorage.setItem("username",data[0][i]['username'])
                        localStorage.setItem("data",JSON.stringify(data[0]))
                      }

            }
         });
         
      event.preventDefault();
      return isLogin;
    }
  
    render() {
      const { loggingIn } = this.props;
      const { username, submitted,isLoginState } = this.state;
            return (
                <div>
                 
                  
                    <Card  variant="outlined">
                      <CardContent>
                        <h1>Digital Tech inc</h1>
                    <hr></hr>
                    <div className="col-md-6 col-md-offset-3  borderTopWidth: '10px'">
                    {!isLoginState &&
                    <div>
                      <form onSubmit={this.handleSubmit}>
                          <label>
                          <TextField id="standard-basic" label="@username" value={this.state.value} onChange={this.handleChange}/>
                            {submitted && 
                              <div className="help-block">Username Requerido</div>
                            }
                          </label>
                            <Button type="submit" variant="contained" color="secondary">
                                Entrar
                            </Button>
                      </form>
                    </div>
                    
                     }
                    {isLoginState && 
                    
                    <div className="col-md-6 col-md-offset-3">
                      <Main username ></Main>
                    </div>
                    }
                    </div>
      </CardContent>
      <CardActions>
        <Button size="small">Terminos</Button>
      </CardActions>
    </Card>
                </div>
                


              );
     
    }
  }
    function mapStateToProps(state) {
        const { loggingIn } = state.authentication;
        return {
            loggingIn
        };
    }
export default Login;