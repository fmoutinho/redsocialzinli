const initialDetails = [
  {
    "id": 1,
    "username": "@fmoutinho",
    "avatar": "../assets/img/1.png",
    "name": "fernando",
    "surname": "moutinho"
  },
  {
    "id": 2,
    "username": "@arodriguez",
    "avatar": "../assets/img/2.png",
    "name": "albert",
    "surname": "rodriguez"
  },
   {
    "id": 3,
    "username": "eparada",
    "avatar": "../assets/img/3.png",
    "name": "eglis",
    "surname": "parada"
  }
  
];

export default initialDetails;