import React from 'react';


function User(props) {
    return (
     <li>{props.name} - {props.email}</li>
    );
} 

export default User;