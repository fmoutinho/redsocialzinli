import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles({
  
  header: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '90px',
    width: '100%'
  },
  gradientBar: {
    width: '100%',
    height: '10px',
    backgroundRepeat: 'no-repeat',
    backgroundSize: '100% 10px',
    backgroundImage: 'linear-gradient(90deg, #00AEC7 27.19%, #0033A0 100%)'
  },
  content: {
    paddingTop: '50px',
    paddingBottom: '50px',
    display: 'inline-flex',
    flexDirection: 'column',
    minHeight: 'calc(100vh - 150px)'
  },
  contentTable: {
    padding: '50px 40px'
  },
  textHeader: {
    color: '#0033A0',
    fontFamily: 'FS Joey',
    fontSize: '22px',
    lineHeight: '28px',
    fontWeight: 'bold'
  },
  textHeaderMobile: {
    textAlign: 'center'
  },
  dataContainer: {
    background: `linear-gradient(90deg, #FFFFFF 43.26%, rgba(255, 255, 255, 0.73) 51.28%, rgba(255, 255, 255, 0) 59.94%), url(${Image})`,
    backgroundSize: 'cover',
    backgroundPosition: '0 0, 0 45%',
    backgroundRepeat: 'no-repeat',
    width: '75%',
    height: '354px',
    margin: '52px auto 50px auto',
    padding: '30px 30px',
    boxShadow: '0px 1px 5px rgba(0, 0, 0, 0.2), 0px 3px 4px rgba(0, 0, 0, 0.12), 0px 2px 4px rgba(0, 0, 0, 0.14)',
    borderRadius: '16px',
    position: 'relative'
  },
  dataContainerMobile: {
    height: '100%',
    width: '100%',
    background: `linear-gradient(90deg, #FFFFFF 43.26%, rgba(255, 255, 255, 0.73) 100%, rgba(255, 255, 255, 0) 100%), url(${Image})`,
    backgroundSize: 'cover',
    backgroundPosition: '0 0, 0 45%',
    backgroundRepeat: 'no-repeat'
  },
  dataContainerTable: {
    width: '100%'
  },
  titleContainer: {
    width: '45%',
    maxWidth: '500px',
    marginBottom: '45px'
  },
  dataText: {
    fontFamily: 'FS Joey',
    fontSize: '20px',
    lineHeight: '23px',
    letterSpacing: '0.15px',
    fontWeight: '500',
    color: '#0033A0'
  },
  titleContainerMobile: {
    width: '100%'
  },
  dataTextMobile: {
    textAlign: 'center',
    display: 'block'
  },
  rutInputMobile: {
    width: '100%'
  },
  cotainerInputMobile: {
    width: 'calc(100% - 64px)',
    marginBottom: '80px'
  },
  button: {
    marginTop: '20px',
    color: '#FFF',
    width: '150px',
    height: '37px',
    fontFamily: 'Barlow',
    fontSize: '14px',
    lineHeight: '17px',
    background: '#00AEC7',
    boxShadow: '0px 1px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.12), 0px 0px 2px rgba(0, 0, 0, 0.14)',
    borderRadius: '20px',
    fontWeight: '400',
    display: 'block',
    '&:hover': {
      background: '#00D7E5'
    },
    '&:disabled': {
      background: '#C4C4C4'
    }
  },
  divider: {
    borderTop: 0,
    margin: '20px -30px',
    borderBottom: '1px solid rgba(0, 0, 0, 0.12)'
  },
  cardBody: {
    '&> h3': {
      color: '#0033A0',
      fontSize: '18px',
      letterSpacing: '0.5px',
      margin: 0
    },
    '&> p': {
      margin: 0,
      color: 'rgba(0, 0, 0, 0.6)',
      fontSize: '14px',
      lineHeight: '24px'
    },
    '&> ul': {
      padding: 0,
      listStyle: 'none',
      margin: '0 -30px',
      '&> li': {
        display: 'block',
        justifyContent: 'space-between',
        padding: '10px 20px',
        '&:first-child': {
          background: '#E7F5F8',
          borderTop: '1px solid #00AEC7',
          borderBottom: '1px solid #00AEC7'
        },
        '&:nth-child(2)': {
          background: 'rgba(0, 0, 0, 0.08)',
          opacity: '0.5'
        },
        '&> span': {
          display: 'block',
          fontSize: '16px'
        }
      }
    }
  },
  dialog: {
    '& .sect-title': {
      borderBottom: '1px solid rgba(0, 0, 0, 0.1)',
      padding: '20px 0px'
    },
    '& .sect-content': {
      minHeight: '150px',
      alignItems: 'center',
      padding: '20px 0px',
      // borderBottom: '1px solid rgba(0, 0, 0, 0.1)',
      '& .valor-comercial': {
        display: 'flex',
        alignItems: 'flex-end',
        padding: '0px 15px',
        '& .title-input-numbers': {
          fontSize: '12px'
        }
      },
      '& .deducible': {
        padding: '0px 15px',
        '& > .sect-input': {
          '& > p': {
            fontSize: '12px'
          },
          '& > .input': {
            width: '100%',
            height: '35px'
          }
        },
        '& > .sect-input2': {
          paddingLeft: '10px',
          alignSelf: 'flex-end',
          '& .title-input-numbers': {
            fontSize: '12px'
          }
        }
      }
    }
  },
  cancelItem: {
    border: '1px solid #00AEC7',
    color: '#00AEC7',
    width: '149px',
    height: '43px',
    padding: '10px !important',
    borderRadius: '20px',
    lineHeight: '24px',
    fontSize: '14px !important',
    textTransform: 'uppercase',
    '& > span': {
      height: '24px !important'
    }
  },container: {
    padding: '10px 0px'
  },
  sectContainerSigners: {
    maxWidth: '436px',
    boxShadow: '0px 1px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.12), 0px 0px 2px rgba(0, 0, 0, 0.14)',
    padding: '10px',
    borderRadius: '8px',
    marginLeft: '27px',
    marginRight: '-16px',
    textAlign: '-webkit-center'

  },
  sectTitleSigners: {
    paddingBottom: '12px',
    borderBottom: '1px solid #00AEC7',
    paddingLeft: '10px',
    '& > .title': {
      color: '#0033A0',
      paddingLeft: '16px'
    }
  },
  sectContainerDataSigners: {
    paddingTop: '12px',
    paddingLeft: '10px'
  },
  sectDataSigners: {
    width: '100%',
    '& .data': {
      width: '50%',
      fontSize: '14px'
    }
  },
  acceptItem: {
    width: '149px',
    height: '43px',
    padding: '10px !important',
    borderRadius: '20px',
    color: 'white !important',
    textTransform: 'uppercase',
    backgroundColor: '#00AEC7 !important',
    lineHeight: '24px',
    fontSize: '14px !important',
    '& > span': {
      height: '24px !important'
    }
  },
  disableAcceptItem: {
    width: '149px',
    height: '43px',
    padding: '10px !important',
    borderRadius: '20px',
    textTransform: 'uppercase',
    lineHeight: '24px',
    backgroundColor: '#72C6D8 !important',
    fontSize: '14px !important',
    color: 'white !important',
    '& > span': {
      height: '24px !important'
    }
  }, iconDocument: {
    color: 'rgba(0, 0, 0, 0.54)'
  },
  buttonContainer: {
    paddingRight: '30px !important',
    borderTop: '1px solid rgba(0,0,0,0.18)'
  },
  letftTitleText: {
    textAlign: 'left !important',
    paddingLeft: '30px',
    paddingTop: '5px',
    paddingBottom: '5px'
  },
  '@global': {
    '.MuiDialog-paperWidthSm': {
      maxWidth: '650px',
      minWidth: '430px',
      background: '#FFFFFF',
      borderRadius: '20px',
      width: '100%',
      height: 'auto'
    },
    '.MuiDialogActions-root': {
      // flex: '0 0 auto',
      // display: 'flex',
      padding: '15px 15px'
      // alignItems: 'center',
      // justifyContent: 'flex-end'
    }
  },
  panel: {
    backgroundSize: 'cover',
    backgroundPosition: '0 0, 0 -110px',
    backgroundRepeat: 'no-repeat',
    width: '100%',
    padding: '30px',
    boxShadow: '0px 1px 5px rgba(0, 0, 0, 0.2), 0px 3px 4px rgba(0, 0, 0, 0.12), 0px 2px 4px rgba(0, 0, 0, 0.14)',
    borderRadius: '16px',
    '& > h2, h4': {
      color: '#0033A0',
      fontFamily: 'FS Joey',
      fontSize: '24px',
      lineHeight: '28px',
      fontWeight: '400',
      margin: '-20px 0 30px 0'
    }
  },
  buttonMobile: {
    margin: 'auto',
    clear: 'both'
  },
  manualLink: {
    textAlign: 'center'
  },
  dontRegisteredLinkText: {
    fontFamily: 'Barlow',
    fontSize: '16px',
    lineHeight: '24px',
    letterSpacing: '0.15px',
    color: '#00aec7',
    cursor: 'pointer',
    display: 'block'
  },
  circleImg: {
    border: '2px solid #0033A0',
    boxSizing: 'border-box',
    borderRadius: '50%',
    width: '70px',
    height: '70px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    float: 'left',
    '&> img': {
      marginTop: '-4px',
      marginLeft: '-1px'
    }
  },
  manualLinkText: {
    fontFamily: 'Barlow',
    fontSize: '16px',
    lineHeight: '24px',
    textDecoration: 'underline',
    letterSpacing: '0.15px',
    color: '#00aec7',
    cursor: 'pointer',
    display: 'block'
  },
  footer: {
    height: '50px',
    width: '100%',
    bottom: 0,
    background: 'linear-gradient(180deg, #0055D5 0%, #121CA6 50.52%)'
  }
})
