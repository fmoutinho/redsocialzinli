/* eslint-disable no-useless-constructor */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect, Fragment } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import Global from './UrlApi';
import Footer from './Footer';
import Navbar from './Navbar';
import Body from './body';
import { Grid, Button, Typography } from '@material-ui/core';


class Main extends React.Component {
 

  constructor(props) {
      super(props);
      this.state = {
        username: '', data :[]
      }
      const Username = localStorage.getItem("username");
      const dataArray = localStorage.getItem("data");
      this.obtenerDataUser();
    }

    obtenerDataUser(){
      this.setState({ username: localStorage.getItem("username")});
      this.setState({data:localStorage.getItem("data")});
    }
  
    render() {
      return (
        
        <div className="col-md-6 col-md-offset-3">
        
          <Navbar/>
    
          <Body username={localStorage.getItem("username")} data={localStorage.getItem("data")}></Body>
          <Footer/>
          
        </div>
      );
    }
  } 
  export default Main;