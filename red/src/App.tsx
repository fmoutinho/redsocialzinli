/* eslint-disable react/jsx-no-undef */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Switch, Route, NavLink } from 'react-router-dom';
import Footer from './Footer';
import Main from './Main.js'
import Login from './NameForm.js';

function App() {
  return (
    <div className="App">
        <Login/>
    </div>
  );
}

interface User {
  avatar?: string;
  username: string;
  name: string;
  surname: string;
}    

export default App;
