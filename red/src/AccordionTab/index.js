/* eslint-disable no-unused-vars */
import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import MuiExpansionPanel from '@material-ui/core/ExpansionPanel'
import MuiExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary'
import MuiExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails'
import { Grid, ExpansionPanel, ExpansionPanelSummary, ExpansionPanelDetails } from '@material-ui/core'
import { useStyles } from './styles'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

const AccordionTab = ({ title, children, color, borderless = false, noRadio = false }) => {
  const classes = useStyles()
  const ExpansionPanel = withStyles({
    root: {
      width: '100%',
      border: '1px solid rgba(0, 0, 0, .125)',
      borderRadius: borderless ? 0 : '16px !important',
      backgroundColor: '#FFF',
      boxShadow: borderless ? null : '0px 2px 4px rgba(0, 0, 0, 0.14), 0px 3px 4px rgba(0, 0, 0, 0.12), 0px 1px 5px rgba(0, 0, 0, 0.2)',
      '&:not(:last-child)': {
        borderBottom: 0
      },
      '&:before': {
        display: 'none'
      },
      '&$expanded': {
        margin: 'auto'
      }
    },
    expanded: {}
  })(MuiExpansionPanel)

  const ExpansionPanelSummary = withStyles({
    root: {
      backgroundColor: '#FFF',
      marginBottom: 0,
      minHeight: 56,
      borderRadius: borderless ? 0 : '16px !important',
      '&$expanded': {
        minHeight: 56,
        borderRadius: borderless ? 0 : '16px',
        borderBottomRightRadius: '0 !important',
        borderBottomLeftRadius: '0 !important'
      }
    },
    content: {
      margin: '0',
      '&$expanded': {
        backgroundColor: '#FFF',
        margin: 0,
        border: 0
      }
    },
    expanded: {
      borderBottom: '1px solid rgba(0, 0, 0, .125)'
    }
  })(MuiExpansionPanelSummary)

  const ExpansionPanelDetails = withStyles(theme => ({
    root: {
      padding: 0
    }
  }))(MuiExpansionPanelDetails)
  return (
    <ExpansionPanel>
      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon style={{ color: color || '#00AEC7' }} />} aria-controls='panel1a-content' id='panel1a-header'>
        {title}
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>{children}</ExpansionPanelDetails>
    </ExpansionPanel>
  )
}

export default AccordionTab
