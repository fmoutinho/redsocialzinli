import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles({
  container: {
    padding: '20px 50px',
    '@media (max-width: 1000px)': {
      padding: '20px 2.5vw'
    }
  }
})
