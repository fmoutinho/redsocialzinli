/* eslint-disable no-unused-vars */
import React from 'react';
import { useStyles } from './styles'

const Footer = ({ likeCount, unlikeCount, all }) => {
  // const { likeCount, unlikeCount, all } = this.props;
  const classes = useStyles()

  return(
    <footer className="footer">
      <small><h6>Desarrollado Por Fernando Moutinho</h6> </small>
    </footer>
  )
}

export default Footer;