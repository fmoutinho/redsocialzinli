import React from 'react'
import PropTypes from 'prop-types'
import { injectIntl } from 'react-intl'
import MaterialTable, { MTableBodyRow, MTableActions, MTableCell } from 'material-table'
import { useStyles } from './styles'
import { Box } from '@material-ui/core'

export const MyMaterialTable = injectIntl(({ tableData, showTitle, detailPanel, tableCellStyle, allowExport = false, intl, responsive = false }) => {
  const classes = useStyles()

  return (
    <MaterialTable
      components={{
        Row: rowProps => <MTableBodyRow {...rowProps} className={!!detailPanel ? {} : classes.row} onRowClick={() => onRowClick(rowProps.data)} />,
        // eslint-disable-next-line react/display-name
        Cell: cellProps => <MTableCell {...cellProps} className={classes.cell} style={tableCellStyle} />,
        // eslint-disable-next-line react/display-name
        Actions: actionsProps => (
          <Box style={{ width: '100%', display: 'flex', justifyContent: 'flex-end', paddingRight: '13px' }}>
            <MTableActions {...actionsProps} />
          </Box>
        )
      }}
      {...tableData}
      options={{
        showTitle,
        filtering: false,
        search: false,
        paging: false,
        sorting: false,
        toolbar: false,
        exportButton: allowExport,
        actionsColumnIndex: -1,
        headerStyle: {
          minWidth: responsive ? 0 : '120px'
        },
        columnsButton: false,
        detailPanelType: 'single',
        detailPanelColumnAlignment: 'right'
      }}
      detailPanel={detailPanel}
      localization={{
        body: {
          emptyDataSourceMessage: 'Sin información para mostrar'
        },
        toolbar: {
          searchTooltip: 'Buscar',
          searchPlaceholder: 'Buscar'
        },
        /* pagination: {
                    labelRowsSelect: intl.formatMessage({ id: 'app.general.itemsPerPage' }),
                    labelDisplayedRows: ' {from}-{to} of {count}',
                    firstTooltip: intl.formatMessage({ id: 'app.containers.service.services.first' }),
                    previousTooltip: intl.formatMessage({ id: 'app.containers.service.services.previous' }),
                    nextTooltip: intl.formatMessage({ id: 'app.containers.service.services.next' }),
                    lastTooltip: intl.formatMessage({ id: 'app.containers.service.services.last' })
                },*/
        header: {
          actions: 'Acciones'
        }
      }}
    />
  )
})

MyMaterialTable.propTypes = {
  tableData: PropTypes.object,
  showTitle: PropTypes.bool
}
