import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles({
  row: {
    '& button': {
      display: 'none'
    },
    '&:hover': {
      '& button': {
        display: 'flex',
        backgroundColor: 'transparent'
      },
      backgroundColor: '#DAF5FF !important'
    },
    transition: 'none !important'
  },
  container: {
    boxShadow: '0px 1px 3px rgba(28, 33, 146, 0.2), 0px 2px 2px rgba(28, 33, 146, 0.12), 0px 0px 2px rgba(28, 33, 146, 0.14)',
    borderRadius: '5px',
    '& > div > div > div > div > table': {
      minWidth: '400px'
    },
    '& ::-webkit-scrollbar-thumb': {
      backgroundColor: '#00aec7',
      border: 0,
      borderRadius: '4px'
    },
    '& ::-webkit-scrollbar-track': {
      backgroundColor: 'rgba(0, 0, 0, 0.08)'
    }
  },
  cell: {
    fontFamily: 'Poppins',
    color: 'rgba(0, 0, 0, 0.38) !important',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap'
  },
  body: {
    width: '100%',
    minWidth: '1200px'
  },
  paginationRoot: {
    '&:last-child': {
      padding: '0 20px',
      width: '100%',
      float: 'left'
    }
  },
  paginationSpacer: {
    flex: 0
  },
  paginationCaption: {
    fontFamily: 'Poppins',
    fontSize: '16px',
    letterSpacing: '0.15px',
    color: 'rgba(0, 0, 0, 0.38)',
    display: 'block',
    '&:nth-of-type(2)': {
      flex: 2,
      textAlign: 'right'
    }
  },
  paginationSelectRoot: {
    marginLeft: '8px',
    fontFamily: 'Poppins',
    fontSize: '16px',
    letterSpacing: '0.15px',
    color: 'rgba(0, 0, 0, 0.38)'
  },
  '@global': {
    'Component-horizontalScrollContainer-2470': {
      color: 'green'
    }
  }
})
