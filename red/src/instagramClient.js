const instagramClient = {
    getUserData: accessToken =>
      new Promise(resolve => {
        fetch(
          `https://api.instagram.com/v1/users/self/?access_token=${accessToken}`
        )
          .then(response => {
            if (response.ok) {
              return response
            }
            throw Error(response.statusText)
          })
          .then(response => response.json())
          .then(data => {
            resolve({ success: true, data })
          })
          .catch(error => {
            resolve({ success: false, error })
          })
      }),
  }
  
  export default instagramClient