import React, { useState, useEffect, Fragment } from 'react';
import { Typography, Grid, Box } from '@material-ui/core'
import Search from './components/Search';
import initialDetails from './data/initialDetails';

const Searchs = (props) => (

    <div>
    <div >
      <Box className='border-box'>
        <Grid container className='title-div' style={{ padding: 15 }}>
        <Typography className='header'>
        <Fragment>
        <div className="tc bg-green ma0 pa4 min-vh-100">
            <Search details={initialDetails}/>
        </div>
        
        </Fragment>

        </Typography>

        </Grid>
      </Box>
    </div>
    </div>
    );
      
export default Searchs;