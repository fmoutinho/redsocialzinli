import React, { useState } from 'react';
import Scroll from './Scroll';
import SearchList from './SearchList';
import { makeStyles } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles((theme) => ({
  margin: {
    margin: theme.spacing(1),
  },
}));

function Search({ details }) {

  const [searchField, setSearchField] = useState("");
  const [searchShow, setSearchShow] = useState(false);
  const classes = useStyles();

  const filteredPersons = details.filter(
    user => {
      return (
        user
        .name
        .toLowerCase()
        .includes(searchField.toLowerCase()) ||
        user
        .username
        .toLowerCase()
        .includes(searchField.toLowerCase())
      );
    }
  );

  const handleChange = e => {
    setSearchField(e.target.value);
    if(e.target.value===""){
      setSearchShow(false);
    }
    else {
      setSearchShow(true);
    }
  };

  function searchList() {
  	if (searchShow) {
	  	return (
	  		<Scroll>
	  			<SearchList filteredPersons={filteredPersons} />
	  		</Scroll>
	  	);
	  }
  }

  return (
    <section className="garamond">
			<div className="pa2">
    <div>
      <div className={classes.margin}>
        <Grid container spacing={1} alignItems="flex-end">
          <Grid item>
            <SearchIcon />
          </Grid>
          <Grid item>
            <TextField id="input-with-icon-grid" label="@Buscar usuario" onChange = {handleChange} />
          </Grid>
        </Grid>
      </div>
    </div>
			</div>
			{searchList()}
		</section>
  );
}

export default Search;