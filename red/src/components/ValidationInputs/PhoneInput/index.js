import React from 'react'
import PhoneInput from 'react-phone-input-2'
import { ValidatorComponent } from 'react-form-validator-core'
import { Box } from '@material-ui/core'
import { styles } from './styles'
import withStyles from 'react-jss'
import clsx from 'clsx'

class PhoneValidatorInput extends ValidatorComponent {
  render() {
    /* eslint-disable no-unused-vars */
    const { classes, inputLabel, errorMessages, validators, requiredError, customLabel, validatorListener, value, ...rest } = this.props

    const { isValid } = this.state
    const required = validators.map(value => (value == 'required' ? true : false))
    return (
      <>
        {inputLabel && (
          <label
            className={clsx(customLabel ? classes.customLabel : classes.label, {
              [classes.errorLabel]: isValid !== true && value != '56'
            })}
          >
            {inputLabel}{' '}
            {required[0] ? (
              <Box component='span' style={{ color: 'red' }}>
                *
              </Box>
            ) : (
              ''
            )}
          </label>
        )}
        <PhoneInput
          {...rest}
          value={value}
          inputProps={{ autoComplete: 'off' }}
          inputClass={clsx(customLabel ? classes.customInput : classes.input, rest.className, {
            [classes.errorInput]: isValid !== true,
            [classes.restInput]: String(value) == ''
          })}
        />
      </>
    )
  }
}

export default withStyles(styles)(PhoneValidatorInput)
