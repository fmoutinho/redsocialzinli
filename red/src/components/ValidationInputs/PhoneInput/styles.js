export const styles = {
  label: {
    fontFamily: 'Barlow',
    fontSize: '12px',
    letterSpacing: '0.4px',
    color: 'rgba(0, 0, 0, 0.38)',
    marginBottom: '5px',
    textAlign: 'left'
  },
  customLabel: {
    fontFamily: 'Barlow !important',
    fontStyle: 'normal !important',
    fontWeight: 'normal !important',
    fontSize: '12px !important',
    lineHeight: '16px !important',
    display: 'flex !important',
    alignItems: 'center !important',
    letterSpacing: '0.4px !important',
    paddingBottom: '5px !important',
    color: 'rgba(0, 0, 0, 0.38) !important'
  },
  customInput: {
    background: '#FFFFFF !important',
    border: '1px solid rgba(0, 0, 0, 0.16) !important',
    boxSizing: 'border-box !important',
    color: 'rgba(0, 0, 0, 0.6) !important',
    borderRadius: '20px !important',
    paddingLeft: '20px !important'
  },
  input: {
    backgroundColor: '#FFF',
    color: 'rgba(0, 0, 0, 0.6)',
    borderRadius: '20px',
    border: '1px solid rgba(0, 0, 0, 0.16)',
    outline: 'none',
    paddingLeft: '10px'
  },
  restInput: {
    color: 'rgba(0, 0, 0, 0.38)'
  },
  errorInput: {
    border: '1px solid #e02020',
    color: '#e02020'
  },
  errorLabel: {
    color: '#e02020'
  }
}
