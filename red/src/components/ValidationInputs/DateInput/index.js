import React, { createRef } from 'react'
import DatePicker, { registerLocale } from 'react-datepicker'
import { ValidatorComponent } from 'react-form-validator-core'
import { Box, Typography } from '@material-ui/core'
import CalendarTodayIcon from '@material-ui/icons/CalendarToday'
import ScheduleIcon from '@material-ui/icons/Schedule'
import { styles } from './styles'
import withStyles from 'react-jss'
import clsx from 'clsx'
import 'react-datepicker/dist/react-datepicker.css'
import es from 'date-fns/locale/es'
registerLocale('es', es)

class DateInput extends ValidatorComponent {
  constructor(props) {
    super(props)
    this.dateInputRef = createRef()
  }

  openCalendar = () => {
    if (!this.props.disabled) {
      this.dateInputRef.current.setState({ focus: true, open: true })
    }
  }

  render() {
    /* eslint-disable no-unused-vars */
    const { classes, inputLabel, value, errorMessages, validators, requiredError, validatorListener, ...rest } = this.props
    const { isValid } = this.state
    const required = validators.map(value => (value == 'required' ? true : false))

    return (
      <>
        <Box className={classes.divDatePicker}>
          {!this.props.showTimeSelectOnly ? (
            <CalendarTodayIcon className={clsx('icon', classes.icon, classes.iconSchedule)} onClick={this.openCalendar} />
          ) : (
            <ScheduleIcon className={clsx('icon', classes.icon)} onClick={this.openCalendar} />
          )}
          {inputLabel && (
            <Typography className={clsx(classes.label, { [classes.errorLabel]: !isValid })}>
              {inputLabel}{' '}
              {required[0] ? (
                <Box component='span' style={{ color: 'red' }}>
                  *
                </Box>
              ) : (
                ''
              )}
            </Typography>
          )}
          <DatePicker
            {...rest}
            ref={this.dateInputRef}
            autoComplete='off'
            onKeyDown={e => e.preventDefault()}
            maxDate={this.props.isMaxDate && new Date()}
            maxTime={this.props.isMaxTime && this.props.maxTime}
            locale='es'
           
            selected={value}
            className={clsx(classes.input, rest.className, { [classes.errorInput]: !isValid })}
          />
        </Box>
      </>
    )
  }
}

export default withStyles(styles)(DateInput)
