export const styles = {
  label: {
    fontFamily: 'Barlow !important',
    fontSize: '12px !important',
    lineHeight: '16px !important',
    letterSpacing: '0.4px !important',
    color: 'rgba(0, 0, 0, 0.38) !important',
    marginBottom: '5px !important',
    textAlign: 'left !important'
  },
  input: {
    backgroundColor: '#FFF',
    color: 'rgba(0, 0, 0, 0.6) !important ',
    fontFamily: 'Barlow',
    fontSize: '16px',
    lineHeight: '20px',
    letterSpacing: '0.25px',
    border: '1px solid rgba(0, 0, 0, 0.16)',
    outline: 'none',
    paddingLeft: '20px',
    '& .react-datepicker-wrapper': {
      width: '100%'
    }
  },
  errorInput: {
    border: '1px solid #e02020',
    color: '#e02020'
  },
  errorLabel: {
    color: '#e02020'
  },
  divDatePicker: {
    position: 'relative',
    marginBottom: '20px',
    '& .icon': {
      position: 'absolute',
      zIndex: '2',
      bottom: '8px',
      right: '14px'
    }
  },
  '@global': {
    '.react-datepicker-wrapper': {
      width: '100%'
    }
  },
  icon: {
    color: 'rgba(0, 0, 0, 0.54)'
  },
  iconSchedule: {
    width: '21px !important'
  }
}
