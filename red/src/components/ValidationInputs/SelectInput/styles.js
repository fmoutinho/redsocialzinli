import arrowDropDown from '../../../assets/img/baseline-arrow_drop_down.png'

export const styles = {
  label: {
    fontFamily: 'Barlow',
    marginBottom: '5px',
    fontSize: '12px',
    letterSpacing: '0.4px',
    color: 'rgba(0, 0, 0, 0.38)',
    textAlign: 'left'
  },
  customLabel: {
    fontFamily: 'Barlow !important',
    fontStyle: 'normal !important',
    fontWeight: 'normal !important',
    fontSize: '12px !important',
    lineHeight: '16px !important',
    display: 'flex !important',
    alignItems: 'center !important',
    letterSpacing: '0.4px !important',
    paddingBottom: '5px !important',
    color: 'rgba(0, 0, 0, 0.38)'
  },
  customInput: {
    border: '1px solid rgba(0, 0, 0, 0.16) !important',
    boxSizing: 'border-box !important',
    color: 'rgba(0, 0, 0, 0.6) !important',
    borderRadius: '20px !important',
    padding: '0 40px 0 20px !important',
    '-webkit-appearance': 'none',
    '-moz-appearance': 'none',
    appearance: 'none',
    background: `url(${arrowDropDown}) 96% / 22px no-repeat #FFF !important`,
    backgroundColor: '#FFF',
    color: 'rgba(0, 0, 0, 0.38) !important',
    outline: 'none',
    fontFamily: 'Barlow !important',
    fontStyle: 'normal !important',
    fontWeight: 'normal !important',
    fontSize: '16px'
  },
  selected: {
    color: 'rgba(0, 0, 0, 0.6) !important'
  },
  input: {
    backgroundColor: '#FFF',
    color: 'black',
    borderRadius: '20px',
    border: '1px solid rgba(0, 0, 0, 0.16)',
    outline: 'none',
    paddingLeft: '10px'
  },
  errorInput: {
    border: '1px solid #e02020 !important',
    color: '#e02020 !important',
    '& option, & optgroup': {
      color: 'black'
    }
  },
  errorInputNotSelected: {
    color: '#ef8888 !important'
  },
  errorLabel: {
    color: '#e02020'
  },

  select: {
    border: '1px solid rgba(0, 0, 0, 0.16)',
    boxSizing: 'border-box !important',
    borderRadius: '20px !important',
    padding: '0 40px 0 20px !important',
    '-webkit-appearance': 'none',
    '-moz-appearance': 'none',
    appearance: 'none',
    background: `url(${arrowDropDown}) 96% / 22px no-repeat #FFF !important`,
    backgroundColor: '#FFF',
    color: 'rgba(0, 0, 0, 0.38)',
    outline: 'none',
    fontFamily: 'Barlow !important',
    fontStyle: 'normal !important',
    fontWeight: 'normal !important',
    fontSize: '16px'
  }
}
