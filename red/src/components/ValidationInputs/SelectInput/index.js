import React from 'react'
import { Box } from '@material-ui/core'
import { ValidatorComponent } from 'react-form-validator-core'
import { styles } from './styles'
import withStyles from 'react-jss'
import clsx from 'clsx'

class SelectInput extends ValidatorComponent {
  render() {
    /* eslint-disable no-unused-vars */
    const { classes, inputLabel, customLabel, errorMessages, validators, requiredError, validatorListener, value, ...rest } = this.props

    const { isValid } = this.state
    const required = validators.map(value => (value == 'required' ? true : false))
    return (
      <>
        {inputLabel && (
          <label className={clsx(customLabel ? classes.customLabel : classes.label, { [classes.errorLabel]: !isValid })}>
            {inputLabel}{' '}
            {required[0] ? (
              <Box component='span' style={{ color: 'red' }}>
                *
              </Box>
            ) : (
              ''
            )}
          </label>
        )}
        <select
          {...rest}
          value={value}
          disabled={this.props.isdisabled === 'true' ? true : null}
          className={clsx(customLabel ? classes.customInput : classes.select, rest.className, {
            [classes.errorInput]: !isValid,
            [classes.errorInputNotSelected]: !isValid && String(value) == '',
            [classes.selected]: String(value) != ''
          })}
        />
      </>
    )
  }
}

export default withStyles(styles)(SelectInput)
