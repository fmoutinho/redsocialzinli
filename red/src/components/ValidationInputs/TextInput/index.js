import React from 'react'
import { Box } from '@material-ui/core'
import { ValidatorComponent } from 'react-form-validator-core'
import { styles } from './styles'
import withStyles from 'react-jss'
import clsx from 'clsx'

class TextInput extends ValidatorComponent {
  render() {
    const { className, classes, customLabel, inputLabel, multiline, validators, labelClassName, validatorlistener, errorMessages, ...rest } = this.props

    const { isValid } = this.state
    const required = validators.map(value => (value == 'required' ? true : false))

    return (
      <>
        {inputLabel && (
          <label className={clsx(customLabel ? classes.customLabel : classes.label, labelClassName, { [classes.errorLabel]: !isValid })}>
            {inputLabel}{' '}
            {required[0] ? (
              <Box component='span' style={{ color: 'red' }}>
                *
              </Box>
            ) : (
              ''
            )}
          </label>
        )}
        {multiline ? (
          <textarea
            rows={10}
            {...rest}
            className={clsx(customLabel ? classes.textAreaCustomInput : classes.textAreaInput, className, {
              [classes.errorInput]: !isValid
            })}
          />
        ) : (
          <input
            {...rest}
            autoComplete='off'
            className={clsx(className, customLabel ? classes.customInput : classes.input, {
              [classes.errorInput]: !isValid
            })}
          />
        )}
      </>
    )
  }
}

export default withStyles(styles)(TextInput)
