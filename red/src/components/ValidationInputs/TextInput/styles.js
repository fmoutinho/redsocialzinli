export const styles = {
  label: {
    fontFamily: 'Barlow',
    marginBottom: '5px',
    fontSize: '12px',
    letterSpacing: '0.4px',
    color: 'rgba(0, 0, 0, 0.38)',
    textAlign: 'left'
  },
  customLabel: {
    fontFamily: 'Barlow !important',
    fontStyle: 'normal !important',
    fontWeight: 'normal !important',
    fontSize: '12px !important',
    lineHeight: '16px !important',
    display: 'flex !important',
    alignItems: 'center !important',
    letterSpacing: '0.4px !important',
    paddingBottom: '5px !important',
    color: 'rgba(0, 0, 0, 0.38) !important'
  },
  customInput: {
    width: '100%',
    maxWidth: '100%',
    background: '#FFFFFF !important',
    border: '1px solid rgba(0, 0, 0, 0.16) !important',
    boxSizing: 'border-box !important',
    color: 'rgba(0, 0, 0, 0.6) !important',
    borderRadius: '20px !important',
    paddingLeft: '20px !important'
  },
  textAreaCustomInput: {
    width: '100%',
    maxWidth: '100%',
    background: '#FFFFFF !important',
    border: '1px solid rgba(0, 0, 0, 0.16) !important',
    height: '70px !important',
    boxSizing: 'border-box !important',
    color: 'rgba(0, 0, 0, 0.6) !important',
    borderRadius: '16px !important',
    paddingLeft: '20px !important'
  },
  input: {
    backgroundColor: '#FFF',
    color: 'rgba(0, 0, 0, 0.6) !important',
    border: '1px solid rgba(0, 0, 0, 0.16)',
    outline: 0,
    paddingLeft: '20px'
  },
  textAreaInput: {
    backgroundColor: '#FFF',
    color: 'rgba(0, 0, 0, 0.6) !important',
    height: '70px !important',
    border: '1px solid rgba(0, 0, 0, 0.16)',
    outline: 0,
    paddingLeft: '20px'
  },
  errorInput: {
    border: '1px solid #e02020 !important',
    color: '#e02020 !important'
  },
  errorLabel: {
    color: '#e02020 !important'
  }
}
