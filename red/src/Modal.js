/* eslint-disable no-restricted-globals */
import React from 'react'
import { Typography, Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@material-ui/core'
import { useStyles } from './styles'
import PropTypes from 'prop-types'
import clsx from 'clsx'

const Modal = ({
  onClose,
  onAccept,
  title,
  children,
  textBtnAccept,
  textBtnCancel,
  openModal,
  disableBtnAccept,
  modalProps,
  hideAcceptButton = false,
  buttonContainerClass
}) => {
  const classes = useStyles()

  const handleClose = () => {
    onClose()
  }

  const handleAccept = () => {
    onAccept()
  }

  const openBool = openModal != null ? openModal : open
  return (
    <Dialog open={openBool} aria-labelledby='form-dialog-title' className={classes.dialog} PaperProps={modalProps}>
      <DialogTitle id='form-dialog-title' className='sect-title'>
        <Typography color='secondary'>{title}</Typography>
      </DialogTitle>
      <DialogContent className='sect-content'>{children}</DialogContent>
      <DialogActions
        className={clsx(classes.buttonContainer, {
          [buttonContainerClass]: Boolean(buttonContainerClass)
        })}
      >
        <Button className={classes.cancelItem} onClick={() => handleClose()}>
          {textBtnCancel}
        </Button>
        {!hideAcceptButton && (
          <Button
            className={disableBtnAccept ? classes.disableAcceptItem : classes.acceptItem}
            disabled={disableBtnAccept}
            variant='contained'
            color='primary'
            size='small'
            onClick={handleAccept}
          >
            {textBtnAccept}
          </Button>
        )}
      </DialogActions>
    </Dialog>
  )
}

Modal.propTypes = {
  onClose: PropTypes.func,
  onAccept: PropTypes.func,
  title: PropTypes.string,
  textBtnCancel: PropTypes.string,
  textBtnAccept: PropTypes.string,
  openModal: PropTypes.bool,
  disableBtnAccept: PropTypes.bool,
  modalProps: PropTypes.any,
  buttonContainerClass: PropTypes.any
}

export default Modal
