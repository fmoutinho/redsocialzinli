export const GLOBAL_MEDIA_QUERIES = {
  small: '(max-width: 599px)',
  medium: '(min-width: 600px) and (max-width: 1024px)',
  ipad: '(min-width: 760px) and (max-width: 770px)',
  large: '(min-width: 1025px)'
}

export default GLOBAL_MEDIA_QUERIES
