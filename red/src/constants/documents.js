export const getBase64 = (file, handleDocument) => {
  let document = ''
  const reader = new FileReader()
  reader.readAsDataURL(file)
  reader.onload = () => {
    document = reader.result
    handleDocument(reader.result)
  }
  reader.onerror = error => {
  }
  return document
}

export const removeAccents = str => {
  return str.normalize('NFD').replace(/[\u0300-\u036f]/g, '')
}

export const DOCUMENTS_FILE_SIZE = 'DOCUMENTS_FILE_SIZE'
export const PHOTOS_FILE_SIZE = 'PHOTOS_FILE_SIZE'
export const AUDIOS_FILE_SIZE = 'AUDIOS_FILE_SIZE'
export const VIDEOS_FILE_SIZE = 'VIDEOS_FILE_SIZE'

const TYPES_FILES = {
  pdf: 'application/pdf',
  jpg: 'image/jpeg',
  png: 'image/png',
  svg: 'image/svg+xml',
  mp3: 'audio/mpeg',
  mp4: 'video/mp4',
  avi: 'video/avi',
  word: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  txt: 'text/plain'
}

export const CATEGORIES_FILES = {
  pdf: DOCUMENTS_FILE_SIZE,
  jpg: PHOTOS_FILE_SIZE,
  png: PHOTOS_FILE_SIZE,
  svg: PHOTOS_FILE_SIZE,
  mp3: AUDIOS_FILE_SIZE,
  mp4: VIDEOS_FILE_SIZE,
  avi: VIDEOS_FILE_SIZE,
  word: DOCUMENTS_FILE_SIZE,
  txt: DOCUMENTS_FILE_SIZE
}

export const selectedTypesFiles = (arrayTypes, ids) => {
  return ids.reduce((acc, item) => {
    return arrayTypes[item] ? acc.concat(arrayTypes[item].map(data => TYPES_FILES[data])) : acc
  }, [])
}

export const findFileType = type => {
  return Object.keys(TYPES_FILES).filter(item => TYPES_FILES[item] === type)[0]
}
