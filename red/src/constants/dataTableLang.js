/* eslint-disable prettier/prettier */
export default {
    noResults: 'No hay registros',
    filterPlaceholder: 'Buscar...',
    rowsPerPageText: 'Filas por página',
    rangeSeparatorText: 'de'
};