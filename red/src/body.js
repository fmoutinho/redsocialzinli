/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/img-redundant-alt */
/* eslint-disable react/style-prop-object */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect, Fragment } from 'react';
import axios from 'axios';
import Global from './UrlApi';
import { useStyles } from './styles'
import Navbar from './Navbar';
import Modal from './Modal';
import { rowStyle, badgeStyle } from './styles'
import { Typography,Badge,Backdrop,Popover,CircularProgress, Grid, Box ,LinearProgress, Tooltip, Tabs, Tab, Divider} from '@material-ui/core'
import iconPerson from './Shape.svg'
import AccordionTab from './AccordionTab/index'
import MyDialog from './MyDialog'
import { TextInput } from './components/ValidationInputs'
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import { red } from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
const useStyless = makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '25ch',
    },
  },
}));

const useStylessss = makeStyles((theme) => ({
  root: {
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
}));

const useStylesss = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
}));

const Body = ({ username, data },props) => {
  const classesss = useStylesss();
  const [open, setOpen] = React.useState(false);

  const [expanded, setExpanded] = React.useState(false);
  // const { likeCount, unlikeCount, all } = this.props;
  const [datalist , setDataList] = useState([]);
  const [datalistAuthor , setDataListAuthor] = useState([]);
  const classess = useStyles();
  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  const handleClick = () => {
    setOpen(true);
  };

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      logout();
      return;
    }

    setOpen(false);
  };
  const classes = useStyles()
  const [confirmModal, setModal] = React.useState(false)
  const [cerrarSession, setCerrarSession] = React.useState(false)
  const [verPerfils, setVerPerfils] = React.useState(false)
  const [confirmModalPerfil, setModalPerfil] = React.useState(false)
  const [commentN, setCommentN] = React.useState('')

  const [deleteModal, setDeleteModal] = React.useState(false)
  const [notifCount, setCountNotif] = useState(0)
  const [anchorEl, setAnchorEl] = useState(null)
  const openNotifications = Boolean(anchorEl)

  const obtenerPost = () => {
    const data = [];
    const arreglo = [];
    const autor=[];
    const publicacion = [];
    var url = Global.urlAvatars;
    var request = "/post";
    axios.get(url + request).then(res => {
      arreglo.push(res.data);
      
      for (let i = 0; i < arreglo.length; i++) {
        if(username===arreglo[i][i]["author"][i]["user"][i]["username"]){
          setDataList(arreglo[i])
          setDataListAuthor(arreglo[i][i]["author"][i]["user"])
        }else{
          <Alert severity="warning">Lo sentimos este author no tiene publicacion!</Alert>
        }
      }

    });
  }

  const handleOpenPublicacion= event => {
    setAnchorEl(event.currentTarget)
  }

  const logout=()=>{
    setCerrarSession(true);
   localStorage.clear();
   localStorage.removeItem('username');
   localStorage.removeItem('data');

  }
  const handleAddPublicacion = () => {
       setModal(true)
  }
  const onClose = () => {
    setModal(false)
    setModalPerfil(false);
  }
 const verPerfil=()=>{
   setVerPerfils(true);
   setModalPerfil(true);
 }
 const handleAcceptModalConfirm = () => {
  <Alert severity="info">Session cerrada!</Alert>  
 }
 const handleCloseModalConfirm = () => {
  <Snackbar open={handleCloseModalConfirm} autoHideDuration={6000} onClose={handleClose}>
  <Alert onClose={handleClose} severity="warning">
  Confirmar!
  </Alert>
</Snackbar>
 }
  useEffect(() => {
    //  const dataUser=[];
    //dataUser.push(obtenerUsers());
    //validarUsuario(dataUser[0]);
    obtenerPost();
  }, [])


  return (
    <div className={classes.cardBody} >
      <div className={'rdt_OptionsTable'} id='options-table'>
          <Tooltip className={classes.betweenIcons} title='Ver detalles' placement='top' onClick={() => logout()}>
              <button type="button" className={classes.logout} onClick={() => verPerfil()} >Ver Perfil</button>
          </Tooltip>
        </div>
        <Box className={`${classes.notificationContent} ${classes.notificationFooter}`}>
                  <CircularProgress size={14} />

                  {verPerfils && datalistAuthor.length > 0
                    ? datalistAuthor.map((data, i) => (
                      <Modal
                      openModal={confirmModalPerfil}
                      onClose={onClose}
                      onAccept={handleAddPublicacion}
                      title={'Perfil'}
                      textBtnAccept='Confirmar'
                      textBtnCancel='Cancelar'
                      disableBtnAccept={false}
                      titleStyle='text-center'
                    >
                        <Grid key={i} className={classes.container}>
                          <Grid container className={classes.sectContainerSigners}>
                            <Grid container className={classes.sectTitleSigners}>
                              <img src={iconPerson} />
                              <Typography className='title'> {username}</Typography>
                            </Grid>
                            <Grid container direction='column' justify='flex-start' alignItems='flex-start' className={classes.sectContainerDataSigners}>
                              <Grid container direction='row' justify='space-between' alignItems='flex-start' className={classes.sectDataSigners}>
                                <Box className='data'>
                                  <Typography className='title'>ID</Typography>
                                </Box>
                                <Box className='data'>
                                  <Typography className='title'>{data.id}</Typography>
                                </Box>
                              </Grid>
                              
                            </Grid>
                            <Grid container direction='column' justify='flex-start' alignItems='flex-start' className={classes.sectContainerDataSigners}>
                              <Grid container direction='row' justify='space-between' alignItems='flex-start' className={classes.sectDataSigners}>
                                <Box className='data'>
                                  <Typography className='title'>AVATAR</Typography>
                                </Box>
                                <Box className='data'>
                                  <Typography className='title'>{data.avatar}</Typography>
                                </Box>
                              </Grid>
                              
                            </Grid>
                            <Grid container direction='column' justify='flex-start' alignItems='flex-start' className={classes.sectContainerDataSigners}>
                              <Grid container direction='row' justify='space-between' alignItems='flex-start' className={classes.sectDataSigners}>
                                <Box className='data'>
                                  <Typography className='title'>NAME</Typography>
                                </Box>
                                <Box className='data'>
                                  <Typography className='title'>{data.name}</Typography>
                                </Box>
                              </Grid>
                              
                            </Grid>
                            <Grid container direction='column' justify='flex-start' alignItems='flex-start' className={classes.sectContainerDataSigners}>
                              <Grid container direction='row' justify='space-between' alignItems='flex-start' className={classes.sectDataSigners}>
                                <Box className='data'>
                                  <Typography className='title'>SURNAME</Typography>
                                </Box>
                                <Box className='data'>
                                  <Typography className='title'>{data.surname}</Typography>
                                </Box>
                              </Grid>
                              
                            </Grid>
                          </Grid>
                        </Grid>
                        </Modal>
                      ))
                    : null}
                      </Box>
                  <Fragment>
                    <Grid container className={classes.center}>
                      <Box className='title'>
                        <Typography className='header'>Author</Typography>
                        <Typography color='primary' className='event'>
                          Digital Tech <i class="fas fa-info-circle    "></i>
                        </Typography>
                      </Box>
                      <Box>
                      {datalistAuthor.length > 0
                            ? datalistAuthor.map((data, i) => (

                                <Grid key={i} className={classes.container}>
                                  <Grid container className={classes.sectContainerSigners}>
                                    <Grid container className={classes.sectTitleSigners}>
                                      <img src={iconPerson} />
                                      <Typography className='title'> {username}</Typography>
                                    </Grid>
                                    <Grid container direction='column' justify='flex-start' alignItems='flex-start' className={classes.sectContainerDataSigners}>
                                      <Grid container direction='row' justify='space-between' alignItems='flex-start' className={classes.sectDataSigners}>
                                        <Box className='data'>
                                          <Typography className='title'>ID</Typography>
                                        </Box>
                                        <Box className='data'>
                                          <Typography className='title'>{data.id}</Typography>
                                        </Box>
                                      </Grid>
                                      
                                    </Grid>
                                    <Grid container direction='column' justify='flex-start' alignItems='flex-start' className={classes.sectContainerDataSigners}>
                                      <Grid container direction='row' justify='space-between' alignItems='flex-start' className={classes.sectDataSigners}>
                                        <Box className='data'>
                                          <Typography className='title'>AVATAR</Typography>
                                        </Box>
                                        <Box className='data'>
                                          <Typography className='title'>{data.avatar}</Typography>
                                        </Box>
                                      </Grid>
                                      
                                    </Grid>
                                    <Grid container direction='column' justify='flex-start' alignItems='flex-start' className={classes.sectContainerDataSigners}>
                                      <Grid container direction='row' justify='space-between' alignItems='flex-start' className={classes.sectDataSigners}>
                                        <Box className='data'>
                                          <Typography className='title'>NAME</Typography>
                                        </Box>
                                        <Box className='data'>
                                          <Typography className='title'>{data.name}</Typography>
                                        </Box>
                                      </Grid>
                                      
                                    </Grid>
                                    <Grid container direction='column' justify='flex-start' alignItems='flex-start' className={classes.sectContainerDataSigners}>
                                      <Grid container direction='row' justify='space-between' alignItems='flex-start' className={classes.sectDataSigners}>
                                        <Box className='data'>
                                          <Typography className='title'>SURNAME</Typography>
                                        </Box>
                                        <Box className='data'>
                                          <Typography className='title'>{data.surname}</Typography>
                                        </Box>
                                      </Grid>
                                      
                                    </Grid>
                                  </Grid>
                                </Grid>

                              ))
                            : null}
                      </Box>
                      <Grid>
                      
                      </Grid>
                      
                    </Grid>
            

    </Fragment>
       
      <div className="col-md-6 col-md-offset-3">
      <button type="button" className={classes.logout} onClick={() => logout()} >
        <span>Logout</span>
         </button>
         <Snackbar open={cerrarSession} autoHideDuration={6000} onClose={handleClose}>
            <Alert onClose={handleClose} severity="success">
            La sesion fue cerrada con exito!
            </Alert>
          </Snackbar>

      <MyDialog
          open={cerrarSession}
          type='session'
          title='Cerrar Session Debes iniciar de nuevo'
          contentText={
            <>
              Haz clic a continuación para ingresar como usuario <br />
              <span style={{ color: '#0033a0' }}>IMPORTANTE: Recuerda permitir ver las pantallas emergentes para poder iniciar sesión en Digital Tech Inc</span>
            </>
          }
          acceptButtonText='Volver a iniciar'
          handleAccept={() => {
            localStorage.clear()
            window.location.reload()
          }}
        />

        <hr className={classes.divider}></hr>
        <h1 className={classes.textHeader}>Hola estimad@ {username}! En esta oportunidad vamos a comenzar a revisar tu Publicacion</h1>
      
      
      </div>
      <div className={classes.cardBody}>
        <button type="button" className={classes.iconDocument} onClick={() => handleAddPublicacion()}>Incluir Publicacion </button>
        <div>
        <Modal
          openModal={confirmModal}
          onClose={onClose}
          onAccept={handleAddPublicacion}
          title={'Añadir Publicacion Digital Tech Inc'}
          textBtnAccept='Confirmar'
          textBtnCancel='Cancelar'
          disableBtnAccept={false}
          titleStyle='text-center'
        >
          
        <div className={classes.section}>
          <div className={classes.topMessage}>
              <Typography className={classes.sectionBody}>
                Esta sera una prueba para registrar una publicacion con un modal, por lo cual se hara el intento.
              </Typography>
          </div>
  <form className={classess.root} noValidate autoComplete="off">
      <div>

      <TextField
          id="standard-number"
          label="ID POST"
          type="number"
          InputLabelProps={{
            shrink: true,
          }}
        />
        <TextField
          id="image"
          label="imaget"
          defaultValue="Imagen"
          helperText="Escribe la ruta de la imagen"
        />
         <TextField
          id="menssage"
          label="menssage"
          defaultValue="Mensaje"
          helperText="menssage"
        />
         <TextField
          id="create"
          label="create"
          defaultValue="fecha"
          helperText="create"
        />
           <TextField
          id="location"
          label="location"
          defaultValue="Location"
          helperText="location"
        />
      </div>
          
           
          </form>
        </div>
      
        </Modal>
        </div>
        <h3  className={classes.textHeader}>Posts</h3>
            <Fragment>
            <Badge color='secondary' badgeContent={datalist.length} max={9} classes={{ root: classes.badgeRoot, badge: classes.badgeText }}>

     
              {datalist.length > 0
               ? datalist.map((data, i) => (
              <Grid key={i}>
              <Card className={classes.root}>
                      <CardHeader
                        avatar={
                          <Avatar aria-label="recipe" className={classes.avatar}>
                            P
                          </Avatar>
                        }
                        action={
                          <IconButton aria-label="settings">
                            <MoreVertIcon />
                          </IconButton>
                        }
                        title={username}
                        subheader="Usuario publico el : September 14, 2016"
                      />
                      <CardMedia
                        className={classes.media}
                        image="./assets/img/1.png"
                        title="Avatar"
                      />
                      <img src={iconPerson}/>

                      <CardContent>
                        <Typography variant="body2" color="textSecondary" component="p">
                          <Typography className='title'>{data.menssage}</Typography>

                        </Typography>
                        <Typography className='title'>{data.create}</Typography>
                      </CardContent>
                      
                      <CardActions disableSpacing>
                        <IconButton aria-label="add to favorites">
                          <FavoriteIcon />
                        </IconButton>
                        
                        <IconButton aria-label="share">
                          <ShareIcon />
                        </IconButton>
                        
                        <IconButton
                          className={clsx(classes.expand, {
                            [classes.expandOpen]: expanded,
                          })}
                          onClick={handleExpandClick}
                          aria-expanded={expanded}
                          aria-label="ver mas"
                        >
                          <ExpandMoreIcon />
                        </IconButton>

                      </CardActions>
                      <Collapse in={expanded} timeout="auto" unmountOnExit>
                        <CardContent>
                          <Typography paragraph>Create:</Typography>
                          <Typography paragraph>
                            Esta es una prueba para zinli
                          </Typography>
                          <Typography paragraph>
                          <Typography className='title'>{data.create}</Typography>

                          </Typography>
                          <Typography paragraph>
                              {data.menssage}
                          </Typography>
                          <Typography>
                            Desarrollado por Fernando Moutinho.
                          </Typography>
                        </CardContent>
                      </Collapse>
                    </Card>
              </Grid>
          ))
        : 
        <Alert severity="error">No existe la data!</Alert>
        }
        </Badge>
    </Fragment>
       
      </div>
    </div>



  )
}

export default Body;