/* eslint-disable jsx-a11y/alt-text */
import React from 'react'
import { Button, Dialog, DialogTitle, DialogContent, DialogActions } from '@material-ui/core'
import { useStyles } from './styles'
import clsx from 'clsx'
import informImage from '../assets/img/informDialogImage.svg'
import warningImage from '../assets/img/warningDialogImage.svg'
import sessionImage from '../assets/img/session.svg'
import signerImage from '../assets/img/signerTitleIMage.svg'

import PropTypes from 'prop-types'
import GLOBAL_MEDIA_QUERIES from '../constants/mediaQueries'

const MyDialog = ({ title, contentText, children, cancelButtonText, acceptButtonText, handleClose, handleAccept, type, open, buttonLine }) => {
  const classes = useStyles()

  return (
    <Dialog open={open} maxWidth={type === 'neutral' || type === 'signer' ? 'xl' : 'sm'} className={classes.proof}>
      {type === 'information' && <img src={informImage} style={ { marginTop: '-35px' }} />}
      {type === 'warning' && <img src={warningImage} style={{ marginTop: '-35px' }} />}
      {type === 'session' && <img src={sessionImage} style={{ marginTop: '35px' }} />}
      {type === 'signer' && (
        <img
          src={signerImage}
      
          style={{ marginTop: '35px' }}
        />
      )}
      <DialogTitle
        className={clsx({
          [classes.marginInfo]: type === 'information',
          [classes.marginWarning]: type === 'warning' || type === 'session' || type === 'signer',
          [classes.neutralHeader]: type === 'neutral'
        })}
      >
        {title}
      </DialogTitle>
      <DialogContent className={clsx({ [classes.neutralContent]: type === 'neutral' })}>
        {contentText && <label className={classes.contentText}>{contentText}</label>}
        {children}
      </DialogContent>
      <DialogActions className={buttonLine ? classes.plainFooter : classes.neutralFooter}>
        {cancelButtonText && (
          <Button className={`${classes.button} ${classes.cancelButton}`} onClick={handleClose}>
            {cancelButtonText}
          </Button>
        )}
        {acceptButtonText && (
          <Button className={`${classes.button} ${classes.acceptButton}`} onClick={handleAccept}>
            {acceptButtonText}
          </Button>
        )}
      </DialogActions>
    </Dialog>
  )
}

MyDialog.propTypes = {
  title: PropTypes.string,
  contentText: PropTypes.any,
  cancelButtonText: PropTypes.string,
  acceptButtonText: PropTypes.string,
  type: PropTypes.string,
  handleClose: PropTypes.func,
  handleAccept: PropTypes.func,
  open: PropTypes.bool,
  buttonLine: PropTypes.any
}

export default MyDialog
