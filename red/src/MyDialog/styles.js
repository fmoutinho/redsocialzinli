import { makeStyles } from '@material-ui/core'

export const useStyles = makeStyles({
  contentText: {
    fontFamily: 'Barlow',
    fontSize: '16px',
    lineHeight: '16px',
    letterSpacing: '0.4px'
  },
  button: {
    width: '150px',
    height: '37px',
    fontFamily: 'Barlow',
    fontSize: '14px',
    lineHeight: 0,
    borderRadius: '20px'
  },
  cancelButton: {
    border: '1px solid #00aec7',
    color: '#00aec7'
  },
  acceptButton: {
    background: '#00AEC7',
    boxShadow: '0px 1px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.12), 0px 0px 2px rgba(0, 0, 0, 0.14)',
    color: '#fff',
    '&:hover': {
      background: '#00D7E5'
    }
  },
  marginInfo: {
    marginTop: '-30px'
  },
  marginWarning: {
    marginTop: '11px'
  },
  neutralHeader: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
    minHeight: '30px',
    marginTop: '19px',
    borderBottom: '1px solid #ddd',
    paddingBottom: '19px !important'
  },
  neutralDialog: {
    maxWidth: '591px !important'
  },
  neutralContent: {
    padding: '0px 0px 0px 30px !important'
  },
  plainFooter: {
    justifyContent: 'center',
    padding: '20px 0px !important',
    borderTopWidth: 0
  },
  neutralFooter: {
    justifyContent: 'center',
    padding: '20px 20px !important',
    borderTop: '1px solid rgba(0,0,0,0.08)'
  },
  '@global': {
    '.MuiDialog-paperWidthSm': {
      maxWidth: '474px',
      height: '420px',
      boxShadow: 'box-shadow: 0px 8px 10px rgba(0, 0, 0, 0.2), 0px 6px 30px rgba(0, 0, 0, 0.12), 0px 16px 24px rgba(0, 0, 0, 0.14)',
      borderRadius: '16px',
      width: '100%'
    },
    '.MuiDialogTitle-root': {
      padding: 0
    },
    '.MuiDialog-paperWidthXl': props => {
      return {
        maxWidth: '675px',
        boxShadow: 'box-shadow: 0px 8px 10px rgba(0, 0, 0, 0.2), 0px 6px 30px rgba(0, 0, 0, 0.12), 0px 16px 24px rgba(0, 0, 0, 0.14)',
        borderRadius: '16px',
        width: '100%',
        paddingTop: '0px !important',
        paddingBottom: '0px !important'
      }
    },
    '.MuiDialogTitle-root .MuiTypography-root': {
      textAlign: 'center',
      fontFamily: 'FS Joey',
      fontSize: '20px',
      lineHeight: '23px',
      color: '#0033A0'
    },
    '.MuiDialogContent-root': {
      textAlign: 'center'
    }
  },
  signerMobile: {
    width: '160px',
    margin: '40px auto 20px'
  },
  signerTablet: {
    margin: '40px auto 50px'
  }
})
